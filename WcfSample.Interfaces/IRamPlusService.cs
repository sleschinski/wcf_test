﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace WcfSample.Interfaces
{
    //SessionMode = SessionMode.Required, CallbackContract = typeof(IProductServiceCallback)
    [ServiceContract()]
    public interface IRamPlusService
    {
        /// <summary>
        /// Closes the application
        /// </summary>
        [OperationContract]
        void ShutDown();
    }
}
