﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Windows.Forms;
using WcfSample.Interfaces;

namespace WcfSample.Host
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class RamPlusService: IRamPlusService
    {
        public void ShutDown()
        {
            string msg = "Client has requested to close application. Continue?";
            if (MessageBox.Show(msg, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
