﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using WcfSample.Interfaces;

namespace WcfSample.Host
{
    public static class HostHelper
    {
        private static ServiceHost _host;

        public static void StartHost()
        {
            var group =
                ServiceModelSectionGroup.GetSectionGroup(
                ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None));

            if (group != null)
            {
                //take the first service and endpoint definition
                var service = group.Services.Services[0];
                var baseAddress = service.Endpoints[0].Address.AbsoluteUri.Replace(service.Endpoints[0].Address.AbsolutePath, String.Empty);

                //create service
                var productService = new RamPlusService();
                
                //create host
                _host = new ServiceHost(productService, new[] { new Uri(baseAddress) });
                _host.AddServiceEndpoint(typeof(IRamPlusService),
                                        new NetTcpBinding(), service.Endpoints[0].Address.AbsolutePath);

                try
                {
                    _host.Open();
                }
                catch (CommunicationObjectFaultedException cofe)
                {
                    //log exception
                }
                catch (TimeoutException te)
                {
                    //log exception
                }
            }
        }
    }
}
