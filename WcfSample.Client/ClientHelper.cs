﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using WcfSample.Interfaces;

namespace WcfSample
{
    public static class ClientHelper
    {
        private static ChannelFactory<IRamPlusService> _pipeFactory;

        public static void Connect(string hostName)
        {
            //get endpoint configuration from app.config.
            var group =
                ServiceModelSectionGroup.GetSectionGroup(
                ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None));

            if (group != null)
            {
                //create duplex channel factory
                var endPoint = group.Client.Endpoints[0];
                string uri = $"net.tcp://{hostName}/RamplusService";

                _pipeFactory = new ChannelFactory<IRamPlusService>(endPoint.Name, new EndpointAddress(uri));
                if (_pipeFactory.Credentials == null) return;

                //_pipeFactory.Credentials.UserName.UserName = $"{hostName}\\1";
                //_pipeFactory.Credentials.UserName.Password = "";
                //_pipeFactory.Credentials.UserName.Password = "myPassword";

                //create a communication channel and register for its events
                var service = _pipeFactory.CreateChannel();
                var channel = service as IClientChannel;
                channel.Faulted += ClientHelper_Faulted;
                channel.Opened += ClientHelper_Opened;

                try
                {
                    //try to open the connection
                    ((IClientChannel)service).Open();
                    
                    //register for added products
                    service.ShutDown();
                }
                catch(Exception ex)
                {
                    //for example show status text or log; 
                    Debug.Print("Failed to conned to service: " + ex.Message);
                }
            }
        }

        private static void ConnectWithServiceClient()
        {
            //var client = new ServiceClient(endPoint.Name, new EndpointAddress(uri));

            //var credentials = client.ClientCredentials.Windows.ClientCredential;
            //credentials.Domain = hostName;
            //credentials.UserName = "1";
            //credentials.Password = "";
        }

        private static void ClientHelper_Opened(object sender, EventArgs e)
        {
            Debug.Print("Client opened.");
        }

        private static void ClientHelper_Faulted(object sender, EventArgs e)
        {
            Debug.Print("Client faulted.");
        }
    }
}
