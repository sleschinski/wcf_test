﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace WcfSample
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var host = WmiUtils.GetConnections().Select(h => h.HostName).FirstOrDefault();

            //foreach (var host in hosts)
            {
                //Debug.Print($"Host name: {host.HostName}; IP: {host.AddressList[0]}");
                ClientHelper.Connect(host);
            }

            Debug.Print("Finished");

            //ClientHelper.Connect();

            //Application.Run(new Form1());
        }
    }
}
