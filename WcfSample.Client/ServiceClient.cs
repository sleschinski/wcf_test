﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using WcfSample.Interfaces;

namespace WcfSample
{
    public class ServiceClient : ClientBase<IRamPlusService>, IRamPlusService
    {
        public ServiceClient()
        {
        }

        public ServiceClient(string endpointConfigurationName) :
            base(endpointConfigurationName)
        {
        }

        public ServiceClient(string endpointConfigurationName, string remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public ServiceClient(string endpointConfigurationName,
            System.ServiceModel.EndpointAddress remoteAddress) :
                base(endpointConfigurationName, remoteAddress)
        {
        }

        public ServiceClient(System.ServiceModel.Channels.Binding binding,
            System.ServiceModel.EndpointAddress remoteAddress) :
                base(binding, remoteAddress)
        {
        }

        public void ShutDown()
        {
            var service = base.InnerChannel as IRamPlusService;
            if (service != null)
            {
                service.ShutDown();
            }
        }
    }
}
