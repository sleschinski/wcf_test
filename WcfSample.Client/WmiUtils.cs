﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Net;

namespace WcfSample
{
    public class WmiUtils
    {
        public static IEnumerable<IPHostEntry> GetConnections()
        {
            var scope = new ManagementScope("\\\\.\\ROOT\\cimv2");

            var query = new ObjectQuery("SELECT * FROM Win32_ServerConnection");

            var searcher = new ManagementObjectSearcher(scope, query);

            var queryCollection = searcher.Get();

            foreach (var item in queryCollection)
            {
                var m = item as ManagementObject;
                if (m != null)
                {
                    var o = m["ComputerName"];

                    // TODO: check if it works with null
                    string s = Convert.ToString(o);     
                    var entry = Dns.GetHostEntry(s);

                    yield return entry;
                }
            }
        }
    }
}
