﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace WcfSample
{
    public static class ArpTest
    {
        public static StreamReader ExecuteCommandLine(String file, String arguments = "")
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.FileName = file;
            startInfo.Arguments = arguments;

            Process process = Process.Start(startInfo);

            return process.StandardOutput;
        }

        public static string IpByMac(string mac)
        {
            mac = mac.Replace('[', ' ');
            mac = mac.Replace(']', ' ');

            var arpStream = ExecuteCommandLine("arp", "-a");

            // Consume first three lines
            for (int i = 0; i < 3; i++)
            {
                arpStream.ReadLine();
            }

            while (!arpStream.EndOfStream)
            {
                var line = arpStream.ReadLine().Trim();

                while (line.Contains("  "))
                {
                    line = line.Replace("  ", " ");
                }

                var parts = line.Split(' ');
                
                // Match device's MAC address 
                if (parts[1].Replace("-", "").ToUpper().Trim() == mac.ToUpper())
                {
                    return parts[0].Trim();
                } 
            }

            return string.Empty;
        }
    }
}
